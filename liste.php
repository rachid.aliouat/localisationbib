<?php

include 'remplirtab.php';

$tab_champs = explode("?", $_SERVER['REQUEST_URI']);

$code_bib = $tab_champs[1];
$url_appel="https://localisationbib.univ-lille.fr/?";

$page="<html lang=\"fr\">\n";
$page.="<head>\n";
$page.="<meta charset=\"utf-8\">\n";
$page.="<title>Liste des localisations</title>\n";
$page.="</head>\n";
$page.="<body>\n";
$page.="<h3>Liste des bibliothèques</h3>";

$page.="<table border=1>\n";
$page.="<tr><td>Code Alma</td><td>Libellé</td><td>Lien intégration Primo url et maps</td><td>Url du site</td></tr>";
foreach ($tab_libelle as $k => $v) {
    $page.= "<tr><td>$k</td> <td>$v</td><td><a href=\".\?$k\" target=\"_blank\" >$url_appel$k</a></td><td><a href=\"$tab_url[$k]\" target=\"_blank\">$tab_url[$k]</a></td></tr>\n";
    //$page.="[$k] $v $tab_url[$k]<br/>\n";
}

$page.="</table>\n";
$page.="</body>\n";
$page.="</html>\n";

echo $page;


?>
