<?php
	//remplir les tableaux associatifs : libelle, url du site de la bib, url google maps iframe
	
$tab_libelle = array("ANG" => "Bibliothèque Angellier");
$tab_url = array("ANG" => "https://bushs.univ-lille.fr/reseau/angellier/");
$tab_maps = array("ANG" => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2530.9275235421387!2d3.1239598161056144!3d50.628463179500386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d63c7ee12969%3A0x127777536bec7c6f!2sUniversit%C3%A9%20de%20Lille%20Campus%20Pont%20de%20Bois!5e0!3m2!1sfr!2sfr!4v1632126474600!5m2!1sfr!2sfr");

$tab_libelle["DEFI"] = "Bibliothèque DEFI-FLE";
$tab_url["DEFI"] = "https://bushs.univ-lille.fr/reseau/defi/";
$tab_maps["DEFI"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2530.9275235421387!2d3.1239598161056144!3d50.628463179500386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d63c7ee12969%3A0x127777536bec7c6f!2sUniversit%C3%A9%20de%20Lille%20Campus%20Pont%20de%20Bois!5e0!3m2!1sfr!2sfr!4v1632126474600!5m2!1sfr!2sfr";

$tab_libelle["VAN"] = "Bibliothèque d'Égyptologie Jacques Vandier";
$tab_url["VAN"] = "https://bushs.univ-lille.fr/reseau/egyptologie/";
$tab_maps["VAN"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2530.9275235421387!2d3.1239598161056144!3d50.628463179500386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d63c7ee12969%3A0x127777536bec7c6f!2sUniversit%C3%A9%20de%20Lille%20Campus%20Pont%20de%20Bois!5e0!3m2!1sfr!2sfr!4v1632126474600!5m2!1sfr!2sfr";

$tab_libelle["IUTA"] = "Bibliothèque de l'IUT A";
$tab_url["IUTA"] = "https://www.iut-a.univ-lille.fr/cr/";
$tab_maps["IUTA"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2531.700568271748!2d3.135307816105199!3d50.61410127949832!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c32831bfb51bab%3A0x52714ad4d035db6f!2sIUT%20A%20de%20Lille!5e0!3m2!1sfr!2sfr!4v1632137134032!5m2!1sfr!2sfr";

$tab_libelle["IUTB"] = "Bibliothèque de l'IUT B";
$tab_url["IUTB"] = "https://bushs.univ-lille.fr/reseau/iut-b/";
$tab_maps["IUTB"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d631.4350507293304!2d3.163859129261405!3d50.7247587987196!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c32f2c1d374a6b%3A0xee55e6b1ec618272!2sIUT%20B%20-%20Universit%C3%A9%20de%20Lille!5e0!3m2!1sfr!2sfr!4v1632067481740!5m2!1sfr!2sfr";

$tab_libelle["IUTC"] = "Bibliothèque de l'IUT C";
$tab_url["IUTC"] = "http://iut-c.univ-lille.fr/fr/bibliotheque-iut-c-de-roubaix.html";
$tab_maps["IUTC"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2527.224126997606!2d3.163569516107562!3d50.697225279510114!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c328ed315fae57%3A0xe9b6dc1f24be0578!2sIUT%20C%20Roubaix!5e0!3m2!1sfr!2sfr!4v1632137069339!5m2!1sfr!2sfr";

$tab_libelle["CRDP"] = "Bibliothèque de recherche Paul Duez";
$tab_url["CRDP"] = "https://crdp.univ-lille.fr/bibliotheque/";
$tab_maps["CRDP"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10124.462050786686!2d3.062812848382237!3d50.624970921996045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d595eb1d9e77%3A0xf2b2dc795904acb7!2sUniversit%C3%A9%20de%20Lille%20-%20Campus%20Moulins!5e0!3m2!1sfr!2sfr!4v1632137203110!5m2!1sfr!2sfr";

$tab_libelle["BSA"] = "Bibliothèque des sciences de l'Antiquité";
$tab_url["BSA"] = "https://bsa.univ-lille3.fr/";
$tab_maps["BSA"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2530.9275235421387!2d3.1239598161056144!3d50.628463179500386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d63c7ee12969%3A0x127777536bec7c6f!2sUniversit%C3%A9%20de%20Lille%20Campus%20Pont%20de%20Bois!5e0!3m2!1sfr!2sfr!4v1632126474600!5m2!1sfr!2sfr";

$tab_libelle["BSTE"] = "Bibliothèque des Sciences de la Terre Enseignement";
$tab_url["BSTE"] = "https://sciences-technologies.univ-lille.fr/centre-revision/";
$tab_maps["BSTE"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2531.9973552904585!2d3.1395669161050597!3d50.608586679497634!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d64d7432cc3b%3A0x975b789ee1f01e43!2sUniversit%C3%A9%20de%20Lille%20%E2%88%92%20Campus%20Cit%C3%A9%20scientifique!5e0!3m2!1sfr!2sfr!4v1632137322183!5m2!1sfr!2sfr";

$tab_libelle["BUSHS"] = "Bibliothèque universitaire Sciences humaines et sociales";
$tab_url["BUSHS"] = "https://bushs.univ-lille.fr/";
$tab_maps["BUSHS"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2530.9275235421387!2d3.1239598161056144!3d50.628463179500386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d63c7ee12969%3A0x127777536bec7c6f!2sUniversit%C3%A9%20de%20Lille%20Campus%20Pont%20de%20Bois!5e0!3m2!1sfr!2sfr!4v1632126474600!5m2!1sfr!2sfr";

$tab_libelle["BUST"] = "Lilliad";
$tab_url["BUST"] = "https://lilliad.univ-lille.fr/services/obtenir-documents-0";
$tab_maps["BUST"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2531.9973552904585!2d3.1395669161050597!3d50.608586679497634!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d64d7432cc3b%3A0x975b789ee1f01e43!2sUniversit%C3%A9%20de%20Lille%20%E2%88%92%20Campus%20Cit%C3%A9%20scientifique!5e0!3m2!1sfr!2sfr!4v1632137322183!5m2!1sfr!2sfr";

$tab_libelle["BUDRO"] = "Bibliothèque de l'Université de Lille, Droit-Gestion";
$tab_url["BUDRO"] = "http://budroitgestion.univ-lille.fr/accueil/";
$tab_maps["BUDRO"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10124.462050786686!2d3.062812848382237!3d50.624970921996045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d595eb1d9e77%3A0xf2b2dc795904acb7!2sUniversit%C3%A9%20de%20Lille%20-%20Campus%20Moulins!5e0!3m2!1sfr!2sfr!4v1632137203110!5m2!1sfr!2sfr";

$tab_libelle["BUSAN"] = "Bibliothèque universitaire de Lille - Santé";
$tab_url["BUSAN"] = "http://busante.univ-lille.fr/accueil/";
$tab_maps["BUSAN"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5064.098856374327!2d3.028979229534031!3d50.60761906798918!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d51b64b236e5%3A0xdfe15c23f1848662!2sBU%20Sant%C3%A9%20Lille!5e0!3m2!1sfr!2sfr!4v1632137735910!5m2!1sfr!2sfr";

$tab_libelle["CHJ"] = "Bibliothèque du Centre d'histoire judiciaire";
$tab_url["CHJ"] = "http://chj.univ-lille2.fr/la-bibliotheque/";
$tab_maps["CHJ"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10124.462050786686!2d3.062812848382237!3d50.624970921996045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d595eb1d9e77%3A0xf2b2dc795904acb7!2sUniversit%C3%A9%20de%20Lille%20-%20Campus%20Moulins!5e0!3m2!1sfr!2sfr!4v1632137203110!5m2!1sfr!2sfr";

$tab_libelle["ENSAIT"] = "Bibliothèque ENSAIT";
$tab_url["ENSAIT"] = "https://www.ensait.fr/lensait/bibliotheque-et-ressources/";
$tab_maps["ENSAIT"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2527.3715846778537!2d3.166086116107507!3d50.694488679509895!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c328e8dc2ac103%3A0x6cf098770dfe0cbe!2sEcole%20Nationale%20Sup%C3%A9rieure%20Des%20Arts%20Et%20Industries%20Textiles!5e0!3m2!1sfr!2sfr!4v1632138858913!5m2!1sfr!2sfr";

$tab_libelle["WEIL"] = "Bibliothèque Eric Weil";
$tab_url["WEIL"] = "https://bushs.univ-lille.fr/reseau/eric-weil/";
$tab_maps["WEIL"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2530.9275235421387!2d3.1239598161056144!3d50.628463179500386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d63c7ee12969%3A0x127777536bec7c6f!2sUniversit%C3%A9%20de%20Lille%20Campus%20Pont%20de%20Bois!5e0!3m2!1sfr!2sfr!4v1632126474600!5m2!1sfr!2sfr";

$tab_libelle["GER"] = "Bibliothèque Etudes Germaniques, Néerlandaises et Scandinaves";
$tab_url["GER"] = "https://bushs.univ-lille.fr/reseau/etudes-germaniques/";
$tab_maps["GER"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2530.9275235421387!2d3.1239598161056144!3d50.628463179500386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d63c7ee12969%3A0x127777536bec7c6f!2sUniversit%C3%A9%20de%20Lille%20Campus%20Pont%20de%20Bois!5e0!3m2!1sfr!2sfr!4v1632126474600!5m2!1sfr!2sfr";

$tab_libelle["ERSO"] = "Bibliothèque Etudes Romanes, Slaves et Orientales";
$tab_url["ERSO"] = "https://bushs.univ-lille.fr/reseau/bibliotheque-etudes-romanes/";
$tab_maps["ERSO"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2530.9275235421387!2d3.1239598161056144!3d50.628463179500386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d63c7ee12969%3A0x127777536bec7c6f!2sUniversit%C3%A9%20de%20Lille%20Campus%20Pont%20de%20Bois!5e0!3m2!1sfr!2sfr!4v1632126474600!5m2!1sfr!2sfr";

$tab_libelle["IRHIS"] = "Bibliothèque Georges Lefebvre";
$tab_url["IRHIS"] = "https://bushs.univ-lille.fr/reseau/irhis-histoire/";
$tab_maps["IRHIS"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2530.9275235421387!2d3.1239598161056144!3d50.628463179500386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d63c7ee12969%3A0x127777536bec7c6f!2sUniversit%C3%A9%20de%20Lille%20Campus%20Pont%20de%20Bois!5e0!3m2!1sfr!2sfr!4v1632126474600!5m2!1sfr!2sfr";

$tab_libelle["BHUMA"] = "Bibliothèque Humanités";
$tab_url["BHUMA"] = "https://bhuma.univ-lille.fr/";
$tab_maps["BHUMA"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2530.9275235421387!2d3.1239598161056144!3d50.628463179500386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d63c7ee12969%3A0x127777536bec7c6f!2sUniversit%C3%A9%20de%20Lille%20Campus%20Pont%20de%20Bois!5e0!3m2!1sfr!2sfr!4v1632126474600!5m2!1sfr!2sfr";

$tab_libelle["LEA"] = "Bibliothèque IMMD-LEA";
$tab_url["LEA"] = "https://bushs.univ-lille.fr/reseau/lea/";
$tab_maps["LEA"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2527.219746178069!2d3.1628329161075657!3d50.697306579510226!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c328ee1e7f9937%3A0xdfbe15e4b27e065!2sUFR%20LEA%20-%20Universit%C3%A9%20de%20Lille!5e0!3m2!1sfr!2sfr!4v1632139062919!5m2!1sfr!2sfr";

$tab_libelle["COM"] = "Bibliothèque Infocom";
$tab_url["COM"] = "https://deccid.univ-lille.fr/infocom/bibliotheque/";
$tab_maps["COM"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2527.6948172153106!2d3.1769525161073253!3d50.68848957950906!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c32892bd294497%3A0x665724e734b87af0!2sBiblioth%C3%A8que%20Infocom%20-%20Universit%C3%A9%20de%20Lille!5e0!3m2!1sfr!2sfr!4v1632139136230!5m2!1sfr!2sfr";

$tab_libelle["SEDUC"] = "Bibliothèque Jacques Hédoux";
$tab_url["SEDUC"] = "https://bushs.univ-lille.fr/reseau/sciences-education/";
$tab_maps["SEDUC"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2530.9275235421387!2d3.1239598161056144!3d50.628463179500386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d63c7ee12969%3A0x127777536bec7c6f!2sUniversit%C3%A9%20de%20Lille%20Campus%20Pont%20de%20Bois!5e0!3m2!1sfr!2sfr!4v1632126474600!5m2!1sfr!2sfr";

$tab_libelle["MIC"] = "Bibliothèque Michelet";
$tab_url["MIC"] = "https://bushs.univ-lille.fr/reseau/michelet/";
$tab_maps["MIC"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2530.9275235421387!2d3.1239598161056144!3d50.628463179500386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d63c7ee12969%3A0x127777536bec7c6f!2sUniversit%C3%A9%20de%20Lille%20Campus%20Pont%20de%20Bois!5e0!3m2!1sfr!2sfr!4v1632126474600!5m2!1sfr!2sfr";

$tab_libelle["BMATH"] = "Bibliothèque Régionale de Recherche en Mathématiques";
$tab_url["BMATH"] = "https://b2rm.univ-lille.fr/";
$tab_maps["BMATH"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2531.9973552904585!2d3.1395669161050597!3d50.608586679497634!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d64d7432cc3b%3A0x975b789ee1f01e43!2sUniversit%C3%A9%20de%20Lille%20%E2%88%92%20Campus%20Cit%C3%A9%20scientifique!5e0!3m2!1sfr!2sfr!4v1632137322183!5m2!1sfr!2sfr";

$tab_libelle["STL"] = "Bibliothèque Savoirs, Textes, Langage";
$tab_url["STL"] = "https://bushs.univ-lille.fr/reseau/savoirs-textes-langage/";
$tab_maps["STL"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2530.9275235421387!2d3.1239598161056144!3d50.628463179500386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d63c7ee12969%3A0x127777536bec7c6f!2sUniversit%C3%A9%20de%20Lille%20Campus%20Pont%20de%20Bois!5e0!3m2!1sfr!2sfr!4v1632126474600!5m2!1sfr!2sfr";

$tab_libelle["SPL"] = "Bibliothèque Sciences Po Lille";
$tab_url["SPL"] = "https://www.sciencespo-lille.eu/bibliotheque-0";
$tab_maps["SPL"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2530.9396517850787!2d3.0638927161056047!3d50.628237879500325!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d591c58035db%3A0x73920aaa6e008778!2sBiblioth%C3%A8que%20de%20Sciences%20Po%20Lille!5e0!3m2!1sfr!2sfr!4v1632139411742!5m2!1sfr!2sfr";

$tab_libelle["STAPS"] = "Bibliothèque STAPS";
$tab_url["STAPS"] = "http://bu.univ-lille2.fr/vos-bibliotheques/les-autres-bucentres-de-doc/bibliotheque-staps/";
$tab_maps["STAPS"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2531.828545951579!2d3.081111116105134!3d50.61172337949806!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d57b5db0c1dd%3A0x464596b4db49eed8!2sBiblioth%C3%A8que%20Staps%20-%20Universit%C3%A9%20de%20Lille!5e0!3m2!1sfr!2sfr!4v1632139540032!5m2!1sfr!2sfr";

$tab_libelle["CLIM"] = "Centrale Lille Imagine";
$tab_url["CLIM"] = "https://centralelille.fr/";
$tab_maps["CLIM"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2532.1370543887806!2d3.1349137161049816!3d50.605990779497255!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d73f92fd3bbb%3A0xe3ab63ef69f45d93!2s%C3%89cole%20Centrale%20de%20Lille!5e0!3m2!1sfr!2sfr!4v1632139635783!5m2!1sfr!2sfr";

$tab_libelle["CDIAE"] = "Centre de documentation IAE";
$tab_url["CDIAE"] = "https://iaelille.fr/vie-etudiante/services-sur-les-campus/";
$tab_maps["CDIAE"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d40514.19740726681!2d3.102083137970862!3d50.605985510163805!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c32a7a42d534e7%3A0x879f974224a7c0a6!2sIAE%20Lille%20University%20School%20of%20Management!5e0!3m2!1sfr!2sfr!4v1632139731773!5m2!1sfr!2sfr";

$tab_libelle["MER"] = "Centre de documentation Master Pro MER";
$tab_url["MER"] = "https://www.univ-lille.fr";
$tab_maps["MER"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2530.9275235421387!2d3.1239598161056144!3d50.628463179500386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d63c7ee12969%3A0x127777536bec7c6f!2sUniversit%C3%A9%20de%20Lille%20Campus%20Pont%20de%20Bois!5e0!3m2!1sfr!2sfr!4v1632126474600!5m2!1sfr!2sfr";

$tab_libelle["CDSES"] = "Centre de documentation Recherche FacSES";
$tab_url["CDSES"] = "https://fasest.univ-lille.fr/le-centre-de-documentation";
$tab_maps["CDSES"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2531.9973552904585!2d3.1395669161050597!3d50.608586679497634!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d64d7432cc3b%3A0x975b789ee1f01e43!2sUniversit%C3%A9%20de%20Lille%20%E2%88%92%20Campus%20Cit%C3%A9%20scientifique!5e0!3m2!1sfr!2sfr!4v1632137322183!5m2!1sfr!2sfr";

$tab_libelle["HCAP"] = "CHRU - Centre Antipoison";
$tab_url["HCAP"] = "https://www.chu-lille.fr/";
$tab_maps["HCAP"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2531.872865393922!2d3.03251441610508!3d50.6108998794978!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d5103447dfd7%3A0xab5633f5f6257936!2sCHU%20de%20Lille!5e0!3m2!1sfr!2sfr!4v1632140200016!5m2!1sfr!2sfr";


$tab_libelle["HCID"] = "CHRU - CIDDES";
$tab_url["HCID"] = "https://www.chu-lille.fr/";
$tab_maps["HCID"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2531.872865393922!2d3.03251441610508!3d50.6108998794978!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d5103447dfd7%3A0xab5633f5f6257936!2sCHU%20de%20Lille!5e0!3m2!1sfr!2sfr!4v1632140200016!5m2!1sfr!2sfr";


$tab_libelle["HDGID"] = "CHRU - Jeanne de Flandre";
$tab_url["HDGID"] = "https://www.chu-lille.fr/";
$tab_maps["HDGID"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2531.872865393922!2d3.03251441610508!3d50.6108998794978!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d5103447dfd7%3A0xab5633f5f6257936!2sCHU%20de%20Lille!5e0!3m2!1sfr!2sfr!4v1632140200016!5m2!1sfr!2sfr";


$tab_libelle["HSAL"] = "CHRU - Roger Salengro";
$tab_url["HSAL"] = "https://www.chu-lille.fr/";
$tab_maps["HSAL"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2531.872865393922!2d3.03251441610508!3d50.6108998794978!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d5103447dfd7%3A0xab5633f5f6257936!2sCHU%20de%20Lille!5e0!3m2!1sfr!2sfr!4v1632140200016!5m2!1sfr!2sfr";


$tab_libelle["ISTNF"] = "Institut de santé au travail du Nord de la France";
$tab_url["ISTNF"] = "https://istnf.fr/";
$tab_maps["ISTNF"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2532.3323951571897!2d3.0363332161048673!3d50.6023607794966!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d503a74da80b%3A0x4b9aba2b1e6d3a0f!2sInstitut%20de%20Sant%C3%A9%20au%20Travail%20du%20Nord%20de%20la%20France%20-%20I.S.T.N.F!5e0!3m2!1sfr!2sfr!4v1632140332582!5m2!1sfr!2sfr";

$tab_libelle["BIST"] = "IST - Centre de ressources documentaires en droit social";
$tab_url["BIST"] = "http://ist.univ-lille2.fr/le-fonds-documentaire.html";
$tab_maps["BIST"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10124.462050786686!2d3.062812848382237!3d50.624970921996045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d595eb1d9e77%3A0xf2b2dc795904acb7!2sUniversit%C3%A9%20de%20Lille%20-%20Campus%20Moulins!5e0!3m2!1sfr!2sfr!4v1632137203110!5m2!1sfr!2sfr";



$tab_libelle["SUAIOCS"] = "SUAIO Cité scientifique";
$tab_url["SUAIOCS"] = "https://www.univ-lille.fr/etudes/sinformer-sorienter/";
$tab_maps["SUAIOCS"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2531.9973552904585!2d3.1395669161050597!3d50.608586679497634!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d64d7432cc3b%3A0x975b789ee1f01e43!2sUniversit%C3%A9%20de%20Lille%20%E2%88%92%20Campus%20Cit%C3%A9%20scientifique!5e0!3m2!1sfr!2sfr!4v1632137322183!5m2!1sfr!2sfr";

$tab_libelle["SUAIOMOULINS"] = "SUAIO Moulins";
$tab_url["SUAIOMOULINS"] = "https://www.univ-lille.fr/etudes/sinformer-sorienter/";
$tab_maps["SUAIOMOULINS"] = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10124.462050786686!2d3.062812848382237!3d50.624970921996045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c2d595eb1d9e77%3A0xf2b2dc795904acb7!2sUniversit%C3%A9%20de%20Lille%20-%20Campus%20Moulins!5e0!3m2!1sfr!2sfr!4v1632137203110!5m2!1sfr!2sfr";

$tab_libelle["SUAIOPDB"] = "SUAIO Pont de Bois";
$tab_url["SUAIOPDB"] = "https://www.univ-lille.fr/etudes/sinformer-sorienter/";
$tab_maps["SUAIOPDB"] = "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1265.4828228969543!2d3.126411!3d50.627755!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb9f1f74edd9dac4a!2sSUAIO%20Universit%C3%A9%20de%20Lille%20Campus%20Pont%20de%20Bois!5e0!3m2!1sfr!2sfr!4v1632133797664!5m2!1sfr!2sfr";

// modele
//$tab_libelle["CODE_BIB"] = "";
//$tab_url["CODE_BIB"] = "";
//$tab_maps["CODE_BIB"] = "";

					
?>
	

